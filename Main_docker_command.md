# docker install :

i) uninstall putative installation 
ii)  docker install 
sudo apt-get update

 sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo apt-get install  docker docker-engine docker.io containerd runc

# add docker permission to current user :

sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chown "$USER":miat /home/"$USER"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R



# docker step by step

# build local docker image
   docker build -f Dockerfile_toulbar2_uai -t uai2022_test0 .

# tag image
   docker tag toulbar_uai_essai1 dallouche/uai2022_test0:toulbar_uai_essai1
# list local images
   docker image list

# login on docker hub repo
   docker login

# push the local image 
   docker push dallouche/uai2022_test0:latest

# get docker from repository 
   docker pull dallouche/uai2022_test0:toulbar_uai_essai1
# 
   docker image list

# run toubar2 uai docker on network.uai instances with network.uai.evid locate in problems directory
   docker run -v $PWD/problems:/code/problem -e "MODEL=/code/problem/network.uai" -e "EVID=/code/problem/network.uai.evid" -e "RESULT=/code/problem/foo.uai.mpe" -e "OPT=-B=2" toulbar_uai_essai1

# stop putative docker process

   docker stop $(docker ps -a -q )

# logout
docker logout


#how to update image on dockerhub :

o) edit containt of the current directory :

i) rebuild new image :
after modification of th contain :
   docker build -f Dockerfile_toulbar2_uai -t uai2022_test0 .

ii) check the tag with
docker image list :
/home/dallouche/uai_eval2022/docker>docker image list
REPOSITORY                       TAG                  IMAGE ID       CREATED          SIZE
dallouche/uai2022_test0          latest               894876627d48   43 minutes ago   574MB
uai2022_test0                    latest               894876627d49   43 minutes ago   574MB

#tag new image with IMAGE ID 
docker tag 894876627d49 dallouche/uai2022_test0
docker login

#push the new release to dockerhub 
docker push dallouche/uai2022_test0


